/*   Author: Lliam Ly
 *   Purpose: Control the hue lights with buttons and learn about /group settings in HUE api
 *   Status: COMPLETE
 *   Date: November 21, 2016
 *   NOTES:
 *    /lights/<target light> (10 commands per second)
 *    /groups <every light> (1 command per second)
 *    
 *    API Webiste
 *        http://192.168.1.20/debug/clip.html
 *       ex:  http://<bridge ip address>/api/1028d66426293e821ecfd9ef1a0731df/lights/1/state
 *       
 *    Developer Site
 *      http://www.developers.meethue.com/documentation/lights-api
 *        
 *    Token
 *        /api/TSqJdEjKABMLd6d4crEiZ7toqKdcq2iR4BTFuMwW
 *        
 *    DEFINING A GROUP (USE POST)
 *      Groups are defined at this url:
 *          /api/TSqJdEjKABMLd6d4crEiZ7toqKdcq2iR4BTFuMwW/groups
 *      With the following body      
 *          {"name": "allLights", "type": "LightGroup", "lights": [ "1","2","3"]}
 *          
 *    CONTROLLING THE GROUP YOU DEFINED  (USE POST)
 *       Controlling Url:
 *          /api/TSqJdEjKABMLd6d4crEiZ7toqKdcq2iR4BTFuMwW/groups/<id>/action
 *       Body:
 *          {"on":true,"bri":20,"sat":254,"hue":53000,"transitiontime":0}
 *          !  for <id> you can use either the group name : "allLights" or the ID number it was 
 *          !  given when you created it, i.e: 1
 *          
 *    DELETING THE GROUP (USE DELETE)
 *          you need to delete by <id> which is a number associated with the name
 *          /api/<username>/groups/<id>
 *         !  Unfortunately, you can only delete groups by the ID number and not by the name you gave it       
 *            
 */

#include <ESP8266WiFi.h>
// TIME GLOBALS
long currTime =0;
long prevTime =0;

// WIFI GLOBALS
const char* ssid     = "The Secrets Of Life";
const char* password = "lliamsrouter";
const char* host = "192.168.1.20";
const int httpPort = 80;

//HUE LIGHT GLOBAL VARIABLES
const int amountOfLights = 3;
String ON_VALUE = "{\"on\":";
String SAT_VALUE="\"sat\":";
String BRI_VALUE="\"bri\":";
String HUE_VALUE="\"hue\":";
String TRANS_VALUE="\"transitiontime\":";
String cmd="";
boolean prevLightStatus[amountOfLights]; 
int colourArray[4]= {0,25000,30000,50000};
boolean groupPrevLightStatus = false; 

//BUTTON GLOBALS
const int BTN1 = 12;
const int BTN2 = 13;
const int BTN3 = 14;
const int BTN4 = 16;

int count[3]={0,0,0}; // a temporary counter for incrementing the colourArray

// Function used to reset all the previous light values
void resetLightValues(boolean resetValue)
{
    for (int x = 0; x<amountOfLights; x++){
        prevLightStatus[x] = resetValue;
    }
}

/* FUNCTION TO SEND HTTP PUT REQUESTS
 *   to use function, you need to pass the following parameters
 *        currLightStatus: (0 is off) (1 is on)
 *        saturation: 0 - 254
 *        brightness: 0 - 254
 *        hue : 0 - 65535
 *        targetLightID = 1 - 3  (controls which light you want to target)
 *        transitiontime: in multiples of 100ms...i.e "transitiontime":10 = 1second
 */ 

void http_PUT_Group(boolean currLightStatus, int sat, int bri, int hue, int transitiontime)  // 5 Parameters -----------------------------GROUP
{
    WiFiClient client;
    
    if (!client.connect(host, httpPort)) {
      Serial.println("connection failed");
      return;
    }
    
    cmd = "{" + SAT_VALUE + sat + ',' + BRI_VALUE + bri + ',' + HUE_VALUE + hue + ',' + TRANS_VALUE + transitiontime + '}';
    
    if (currLightStatus != groupPrevLightStatus)
    {
        if (currLightStatus == true)
        { //If you are turning it on
            resetLightValues(true);
            groupPrevLightStatus = currLightStatus;
            cmd.remove(0,1);
            cmd = ON_VALUE + "true," + cmd;
        }
        else if (currLightStatus == false)
        { // If you are  turning it off 
            resetLightValues(false);
            groupPrevLightStatus = currLightStatus;
            cmd.remove(0,1);
            cmd = ON_VALUE + "false," + cmd;    
        } 
    }
    
     //HTTP 1.1 FORMATED WITH OUR GENERATED TOKEN AND CMD
     Serial.print("Sending Commands: "); Serial.println(cmd);
     client.print("PUT /api/TSqJdEjKABMLd6d4crEiZ7toqKdcq2iR4BTFuMwW");client.print("/groups/1"); //groups/1 defined to control lights 1-3
     client.println("/action HTTP/1.1");
     client.println("Close");
     client.println("Host: 192.168.1.20");
     client.print("Content-Length: "); client.println(cmd.length());
     client.println("Content-Type: text/plain;charset=UTF-8");
     client.println(); 
     client.println(cmd); 
     Serial.println();
}

void http_PUT_Individual(boolean currLightStatus,int sat, int bri, int hue,int targetLightID) // 5 parameters --------------------------INDIVIDUAL
{
    WiFiClient client;
    
    if (!client.connect(host, httpPort)) {
      Serial.println("connection failed");
      return;
    }

    groupPrevLightStatus = currLightStatus; // so if 2 lights are on, and you hit the 4th button it turns the 2 lights off
  
    // COMBINING ALL OUR PASS PARAMETERS INTO ONE STRING
    cmd = "{" + SAT_VALUE + sat + ',' + BRI_VALUE + bri + ',' + HUE_VALUE + hue + '}';
  
    // OPTIMIZATING THE CMD STATEMENT: COMPARE THE CURRENT STATE OF THE LIGHT WITH THE PAST STATE. IF YOU AREN'T 
    // CHANGING THE STATES YOU DON'T NEED TO SEND THE ON COMMAND AGAIN. 
     
    if (targetLightID == 1)
    { //If the current state of the light is not equal to the the past state 
        if (currLightStatus != prevLightStatus[0])
        {
            if (currLightStatus == true)
            { //If you are turning it on
                prevLightStatus[0] = currLightStatus;
                cmd.remove(0,1);
                cmd = ON_VALUE + "true," + cmd;
            }
            else if (currLightStatus == false)
            { // If you are  turning it off 
                prevLightStatus[0] = currLightStatus;
                cmd.remove(0,1);
                cmd = ON_VALUE + "false," + cmd;
            } 
        }
    }
    else if (targetLightID == 2)
    {
        if (currLightStatus != prevLightStatus[1])
        {
            if (currLightStatus == true)
            { //If it is on
                prevLightStatus[1] = currLightStatus;
                cmd.remove(0,1);
                cmd = ON_VALUE + "true," + cmd;
            }
            else if (currLightStatus == false)
            {
                prevLightStatus[1] = currLightStatus;
                cmd.remove(0,1);
                cmd = ON_VALUE + "false," + cmd;
            } 
        }  
    }
  
    else if (targetLightID == 3)
    {
        if (currLightStatus != prevLightStatus[2])
        {
            if (currLightStatus == true)
            {
                prevLightStatus[2] = currLightStatus;
                cmd.remove(0,1);
                cmd = ON_VALUE + "true," + cmd;
            }
            else if (currLightStatus == false)
            {
                prevLightStatus[2] = currLightStatus;
                cmd.remove(0,1);
                cmd = ON_VALUE + "false," + cmd;
            } 
        }
    }
    
     //HTTP 1.1 FORMATED WITH OUR GENERATED TOKEN AND CMD
     Serial.print("Sending Commands: "); Serial.println(cmd);
     client.print("PUT /api/TSqJdEjKABMLd6d4crEiZ7toqKdcq2iR4BTFuMwW");client.print("/lights/");client.print(targetLightID);
     client.println("/state HTTP/1.1");
     client.println("Close");
     client.println("Host: 192.168.1.20");
     client.print("Content-Length: "); client.println(cmd.length());
     client.println("Content-Type: text/plain;charset=UTF-8");
     client.println(); 
     client.println(cmd); 
     Serial.println();
}

void check_Button(){  //----------------------------------------------------------------------------------------------------------CHECKBUTTON
    int btnState1 = digitalRead(BTN1);
    int btnState2 = digitalRead(BTN2);
    int btnState3 = digitalRead(BTN3);
    int btnState4 = digitalRead(BTN4);
    int brightness = 20;

    currTime= millis();
    
    if ((currTime-prevTime) > 150)
    {
        prevTime=millis();
        
        if (btnState1 == LOW){
           count[0]++;
           Serial.println("scenario 1");
           if (count[0] > 3)
              count[0]=0;
           http_PUT_Individual(true,254,brightness,colourArray[count[0]],1);
        }
        
        else if (btnState2 == LOW){
           Serial.println("scenario 2"); 
           count[1]++;         
           if (count[1] > 3)
              count[1]=0;
           http_PUT_Individual(true,254,brightness,colourArray[count[1]],2);         
        }
        else if (btnState3 == LOW){
           Serial.println("scenario 3");
           count[2]++;
           if (count[2] > 3)
              count[2]=0;
           http_PUT_Individual(true,254,brightness,colourArray[count[2]],3);
        }
        else if (btnState4 == LOW){
           Serial.println("scenario 4");
           if (groupPrevLightStatus==true)
                http_PUT_Group(false,0,0,0,20);
           else if (groupPrevLightStatus == false)
                http_PUT_Group(true,0,60,45000,30);
              
        }         
    }
}

void setup()  // ------------------------------------------------------------------------------------------------------------------------SETUP
{
    Serial.begin(115200);
    delay(100);

    //BUTTON SETUP
    pinMode(BTN1,INPUT);
    pinMode(BTN2,INPUT);
    pinMode(BTN3,INPUT);
    pinMode(BTN4,INPUT);
    
    // CONNECTING TO WIFI
    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
    
    WiFi.begin(ssid, password);
    
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
  
    Serial.println("");
    Serial.println("WiFi connected");  
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

    //resetting light values
    resetLightValues(false);
    http_PUT_Group(false,0,0,0,20);
    
}

void loop() // -----------------------------------------------------------------------------------------------------------------------LOOP
{
  check_Button();
}
